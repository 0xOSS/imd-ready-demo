/**
 * Created by hakansvalin on 2017-05-31.
 */
$(function(document){
    "use strict";

    Highcharts.chart('consumptionOverview', {
        chart: {
            type: 'column',
            width: '500'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['Tis', 'Ons', 'Tor', 'Fre', 'Lö', 'Sö', 'Må']
        },
        yAxis: [
            {
                min: 0,
                title: {
                    text: 'kWh'
                }
            }
        ],
        legend: {
            shadow: false
        },
        tooltip: {
            shared: false
        },
        plotOptions: {
            column: {
                grouping: false,
                shadow: false,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Denna vecka',
            color: 'rgba(165,170,217,1)',
            data: [81, 73, 76, 77.2, 79, 81, 81.4],
            pointPadding: 0.3,
            pointPlacement: -0.2
        }, {
            name: 'Samma vecka i fjol',
            color: 'rgba(126,86,134,.9)',
            data: [74, 78, 80, 79, 81, 76, 77],
            pointPadding: 0.4,
            pointPlacement: -0.2
        }]
    });

});