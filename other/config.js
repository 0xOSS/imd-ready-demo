/**
 *
 * @module config.js
 * @author H Svalin (PE)
 */
var sva = sva || {};
if ( sva.config ) {
    throw new Error('Bad, sva already has the property config');
} else {
    sva.requirejs_config = {
        baseUrl: '../node_modules',
        paths: {
            jquery: "zurb-foundation-6-prebuilt/js/vendor/jquery",
            foundation: "zurb-foundation-6-prebuilt/js/vendor/foundation",
            vue: "vue/dist/vue",
            highcharts: [
                "highcharts/highcharts",
                "highcharts/highcharts-more",
                "highcharts/modules/exporting"
            ],
            "common/series": "../other/js/common/series"
        },
        shim: {
            foundation: {
                deps: ['jquery'],
                exports: 'Foundation'
            },
            "foundation.equalizer": ["foundation"],
            vue: {
                exports: "Vue"
            }
        }

    };
}