/**
 *
 * @module series
 * @author H Svalin (PE)
 */

define(['jquery'], function($) {
    "use strict";

    /**
     *
     * @param {Object} data - Assumed to be a Weather data object as defined by the
     *      weather endpoint over at Azure.
     * @param {String} category - Defaults to temperatures
     * @return {Number[]}
     */
    function weatherDaySeries(data, category) {
        category = category || 'temperatures';
        var result = [];
        if ( data && data.data ) {
            $.each(data.data[0][category], function (k, v) {
                result[parseInt(k)] = v;
            });
        }
        return result;
    }

    return {
        weatherDaySeries: weatherDaySeries
    }
});