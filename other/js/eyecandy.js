/**
 *
 * @module eyecandy
 * @author H Svalin (PE)
 */
requirejs.config({
    baseUrl: 'lib',
    paths: {
        jquery: "jquery/dist/jquery",
        foundation: "foundation-sites/dist/js/foundation",
        "foundation.equalizer": "foundation-sites/dist/js/plugins/foundation.equalizer"
    },
    shim: {
        foundation: {
            deps: ['jquery'],
            exports: 'Foundation'
        },
        "foundation.equalizer": ["foundation"]
    }
});


requirejs(['foundation', 'foundation.equalizer', 'jquery'], function(F, E, $){
    "use strict";

    $(document).foundation();

    console.log('I\'m loaded; eyecandy');
});