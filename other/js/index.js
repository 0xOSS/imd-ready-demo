/**
 *
 * @module index.js
 * @author H Svalin (PE)
 */

requirejs.config(sva.requirejs_config);

requirejs(['foundation', 'jquery'], function(F, $){
    "use strict";

    $(document).foundation();

    console.log('I\'m loaded; index');
});