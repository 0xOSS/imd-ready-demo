/**
 *
 * @module temperatures.js
 * @author H Svalin (PE)
 */

requirejs.config(sva.requirejs_config);

require(['jquery', 'vue', 'common/series', 'foundation', 'highcharts'], function($, V, series){
    "use strict";

    var tempChart, vue, today = new Date().toISOString();

    // Load/Update temperature chart with data from endpoint
    function loadTemperatureChart(chart, location, date) {
        var opts = {
                url: "https://brikks-mediator-laborations.azurewebsites.net/api/weather/" + location,
                method: 'GET',
                data: {
                    code: "lWEpNeMTmXxX9NUoKr9rSHDoNxezfXofYnzrJD1fuu1rxwM4yEGHqw==",
                    dates:date
                }
            },
            chartOpts = null;

        $.ajax(opts).done(function(data) {
            if ( data && data.data ) {
                chartOpts = {
                    name: data.meta.location.name,
                    data: series.weatherDaySeries(data)
                };
                chart.series[0].update(chartOpts);
            }
        });
    }

    tempChart = Highcharts.chart('chart-holder', tempchartOptions);

    vue = new V({
        el: "#chartControlPanel",
        methods: {
            refreshChart: function(){
                loadTemperatureChart(tempChart, this.chartLocation, this.chartDate);
            },
        },
        data: {
            chartLocation: 2693555,
            chartDate: today.substring(0, today.indexOf('T')),
            locations: [
                {
                    "name": "Lund",
                    "id": 2693555
                },
                {
                    "name": "Växjö",
                    "id": 2663536
                },
                {
                    "name": "Orsa",
                    "id": 2686380
                },
                {
                    "name": "Landskrona",
                    "id": 2697719
                },
                {
                    "name": "Nybro",
                    "id": 2687897
                },
                {
                    "name": "Hässleholm",
                    "id": 2707396
                }
            ]
        }
    });

    $(document).foundation();
    loadTemperatureChart(tempChart, vue.chartLocation, vue.chartDate);
    console.log('I\'m loaded; temperatures');
});

var tempchartOptions = {
    chart: {
        type: 'spline'
    },
    title: {
        text: 'Selected days temperatures'
    },
    subtitle: {
        text: 'Source: OpenWeatherMap (via BRIKKS)'
    },
    xAxis: {
        categories: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00',
            '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00']
    },
    yAxis: {
        title: {
            text: 'Temperature (°C)'
        },
        labels: {
            formatter: function () {
                return this.value + '°';
            }
        }
    },
    tooltip: {
        crosshairs: true,
        shared: true,
        valueSuffix: ' °C'
    },
    plotOptions: {
        spline: {
            marker: {
                radius: 4,
                lineColor: '#666666',
                lineWidth: 1
            }
        }
    },
    series: [{
        name: 'Pick a Location above...',
        marker: {
            symbol: 'circle'
        },
        data: []
    }]
};